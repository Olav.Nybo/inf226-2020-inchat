package inf226.inchat;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EncoderTest {

    /** Danger symbols that should be excluded.
     *
     * '&' is excluded because encoding converts some sign to codes that start
     * with '&'.
     */
    private final char[] dangerSymbols = {'<', '>', '"', '\'', '/'};


    /**
     * Tests that dangerous symbols are removed from html code.
     */
    @Test
    public void encodingRemovesDangerSymbols() {
        String toBeEncoded = "<script src=\"some_value\">GENERIC VALUES</script>";
        String afterEncoding = EncoderUtil.forHtml(toBeEncoded);

        for (char symbol : dangerSymbols) {
            assertFalse(afterEncoding.contains(String.valueOf(symbol)),
                    "[  ERROR  ] String contains danger symbol \'" + symbol + "\'.");
        }
    }
}
