package inf226.inchat;

import inf226.storage.Stored;
import inf226.util.Pair;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {

    @Test
    void checkPassword() {
        // Not recommended passwords, only for testing
        String password1 = "abcdefgh12345";
        String password2 = "12345abcdedfgh";

        Account account = Account.create(
                new Stored<>(new User(null, null)),
                password1
        );

        assertTrue(account.checkPassword(password1));
        assertFalse(account.checkPassword(password2));
    }
}