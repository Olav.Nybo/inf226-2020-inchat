package inf226.inchat;

import inf226.util.Pair;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import static inf226.inchat.HashUtil.getHasher;
import static inf226.inchat.HashUtil.hash;

/**
 * This class mainly tests pre-known values.
 */
class HashUtilTest {

    /** Known values, these should NOT be used as password, they are just for testing. */
    Pair<char[], String> known1 = new Pair<>(
        "abcdefgh".toCharArray(),
        "$argon2id$v=19$m=32768,t=10,p=1$ib9V8pyTkLezGORKXsFFug$b5WUnoVLDPGx/fpikigRlks7TyMFTH0TeUZxLEhW4TQ"
    );
    Pair<char[], String> known2 = new Pair<>(
            "fjjj4klh8f9aasdlfkj".toCharArray(),
            "$argon2id$v=19$m=32768,t=10,p=1$kdM/HEhTmnQHvayaEwqahQ$kEnL3EJEaHoGeBVC3xU9rXW1dTW4xV5kzb+pMTrOEqo"
    );

    /**
     * Right now the salt is auto-generated, so hash() is difficult to test.
     * Only test for not null at this time.
     */
    @Test
    void testHashNotNull() {
        assertNotNull(hash(known1.first));
        assertNotNull(hash(known2.first));
    }

    @Test
    void verify() {
        assertTrue(getHasher().verify(known1.second, known1.first));
        assertTrue(getHasher().verify(known2.second, known2.first));
    }
}