package inf226.inchat;

import inf226.storage.DeletedException;
import inf226.storage.Stored;
import inf226.util.Maybe;
import inf226.util.Pair;
import inf226.util.Util;
import inf226.util.immutable.List;

import java.rmi.NoSuchObjectException;
import java.sql.SQLException;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.function.Consumer;

/**
 * This class models the chat logic.
 *
 * It provides an abstract interface to
 * usual chat server actions.
 *
 * TODO: This might be a good place to start implementing
 *       access control.
 **/

public class InChat {
    private final UserStorage userStore;
    private final ChannelStorage channelStore;
    private final AccountStorage accountStore;
    private final SessionStorage sessionStore;
    private final Map<UUID,List<Consumer<Channel.Event>>> eventCallbacks
        = new TreeMap<UUID,List<Consumer<Channel.Event>>>();

    /** Storing channel, and their users w/roles.
     *
     * Stored in format <ChannelName, <UserName, Role>>
     */
    private final HashMap<String, HashMap<String, Role>> channelUsers;

    public InChat(UserStorage userStore,
                  ChannelStorage channelStore,
                  AccountStorage accountStore,
                  SessionStorage sessionStore) {
        this.userStore=userStore;
        this.channelStore=channelStore;
        this.accountStore=accountStore;
        this.sessionStore=sessionStore;
        this.channelUsers = new HashMap<>();
    }


    /**
     * Log in a user to the chat.
     */
    public Maybe<Stored<Session>> login(String username, String password) {
        try {
            final Stored<Account> account = accountStore.lookup(username);
            if (account.value.checkPassword(password)) {  // if password is correct
                System.err.println("[  INFO  ] Password OK, login successful.");
                final Stored<Session> session = sessionStore.save(new Session(account, Instant.now().plusSeconds(60 * 60 * 24)));
                return Maybe.just(session);
            } else {
                System.err.println("[  WARNING  ] Password not OK, login unsuccessful.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (DeletedException e) {
            e.printStackTrace();
        }
        return Maybe.nothing();
    }
    
    /**
     * Register a new user.
     */
    public Maybe<Stored<Session>> register(String username, String password) {
        // First; check that no other users has the same username
        try {
            if (!userStore.lookup(username).equals(Maybe.nothing())) {  // if there is a similar username
                System.err.println("[  ERROR  ] A user with the username " + username + " already exists!");
                return Maybe.nothing();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            final Stored<User> user =
                    userStore.save(User.create(username));
            final Stored<Account> account =
                    accountStore.save(Account.create(user, password));
            final Stored<Session> session =
                    sessionStore.save(new Session(account, Instant.now().plusSeconds(60 * 60 * 24)));

            System.out.println("[  INFO  ] User " + username + " successfully registered.");
            return Maybe.just(session);
        } catch (SQLException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Restore a previous session.
     */
    public Maybe<Stored<Session>> restoreSession(UUID sessionId) {
        try {
            return Maybe.just(sessionStore.get(sessionId));
        } catch (SQLException e) {
            System.err.println("When restoring session:" + e);
            return Maybe.nothing();
        } catch (DeletedException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Log out and invalidate the session.
     */
    public void logout(Stored<Session> session) {
        try {
            Util.deleteSingle(session,sessionStore);
        } catch (SQLException e) {
            System.err.println("When loging out of session:" + e);
        }
    }
    
    /**
     * Create a new channel.
     */
    public Maybe<Stored<Channel>> createChannel(Stored<Account> account,
                                                String name) {
        try {
            Stored<Channel> channel
                = channelStore.save(new Channel(name,List.empty()));

            // Setting creator to owner & testing the invariant
            setRole(channel.value, account.value.user.value, Role.OWNER);
            assertHasOwner(channel.value);

            return joinChannel(account, channel.identity);
        } catch (SQLException e) {
            System.err.println("When trying to create channel " + name +":\n" + e);
        } catch (NoSuchObjectException e) {
            System.err.println("[ ERROR ] No owners for channel.");
        }
        return Maybe.nothing();
    }
    
    /**
     * Join a channel.
     */
    public Maybe<Stored<Channel>> joinChannel(Stored<Account> account,
                                              UUID channelID) {
        try {
            Stored<Channel> channel = channelStore.get(channelID);

            // Checking that account is not banned from channel
            if (isBanned(channel.value, account.value.user.value) == false) {
                Util.updateSingle(account,
                        accountStore,
                        a -> a.value.joinChannel(channel.value.name, channel));
                Stored<Channel.Event> joinEvent
                        = channelStore.eventStore.save(
                        Channel.Event.createJoinEvent(Instant.now(),
                                account.value.user.value.name.getName()));
                return Maybe.just(
                        Util.updateSingle(channel,
                                channelStore,
                                c -> c.value.postEvent(joinEvent)));
            } else {
                System.err.println("[ ERROR ] User can't join channel because of ban.");
            }
        } catch (DeletedException e) {
            // This channel has been deleted.
        } catch (SQLException e) {
            System.err.println("When trying to join " + channelID +":\n" + e);
        }
        return Maybe.nothing();
    }
    
    /**
     * Post a message to a channel.
     */
    public Maybe<Stored<Channel>> postMessage(Stored<Account> account,
                                              Stored<Channel> channel,
                                              String message) {
        try {
            // Checking that user is allowed to post messages in channel
            if (canPost(channel.value, account.value.user.value)) {

                Stored<Channel.Event> event
                        = channelStore.eventStore.save(
                        Channel.Event.createMessageEvent(Instant.now(),
                                account.value.user.value.name.getName(), message));
                try {
                    return Maybe.just(
                            Util.updateSingle(channel,
                                    channelStore,
                                    c -> c.value.postEvent(event)));
                } catch (DeletedException e) {
                    // Channel was already deleted.
                    // Let us pretend this never happened
                    Util.deleteSingle(event, channelStore.eventStore);
                }
            } else {
                System.err.println("[ ERROR ] User doesn't have permission to post.");
            }
        } catch (SQLException e) {
            System.err.println("When trying to post message in " + channel.identity +":\n" + e);
        }
        return Maybe.nothing();
    }
    
    /**
     * A blocking call which returns the next state of the channel.
     */
    public Maybe<Stored<Channel>> waitNextChannelVersion(UUID identity, UUID version) {
        try {
            return Maybe.just(channelStore.waitNextVersion(identity, version));
        } catch (SQLException e) {
            System.err.println("While waiting for the next message in " + identity +":\n" + e);
        } catch (DeletedException e) {
            // Channel deleted.
        }
        return Maybe.nothing();
    }
    
    /**
     * Get an event by its identity.
     */
    public Maybe<Stored<Channel.Event>> getEvent(UUID eventID) {
        try {
            return Maybe.just(channelStore.eventStore.get(eventID));
        } catch (SQLException e) {
            return Maybe.nothing();
        } catch (DeletedException e) {
            return Maybe.nothing();
        }
    }
    
    /**
     * Delete an event.
     */
    public Stored<Channel> deleteEvent(Stored<Channel> channel, Stored<Channel.Event> event) {
        try {
            // Checking that a user is allowed to delete events
            User user = userFromName(event.value.sender);
            if (canDeleteAll(channel.value, user)) {

                Util.deleteSingle(event, channelStore.eventStore);
                return channelStore.noChangeUpdate(channel.identity);
            } else {
                System.err.println("[ ERROR ] User is not allowed to delete.");
            }
        } catch (SQLException er) {
            System.err.println("While deleting event " + event.identity +":\n" + er);
        } catch (DeletedException er) {
        }
        return channel;
    }

    /**
     * Edit a message.
     */
    public Stored<Channel> editMessage(Stored<Channel> channel,
                                       Stored<Channel.Event> event,
                                       String newMessage) {
        // Checking that a user is allowed to edit
        User user = userFromName(event.value.sender);
        if (canEditAll(channel.value, user)) {
            try {
                Util.updateSingle(event,
                        channelStore.eventStore,
                        e -> e.value.setMessage(newMessage));
                return channelStore.noChangeUpdate(channel.identity);
            } catch (SQLException er) {
                System.err.println("While deleting event " + event.identity + ":\n" + er);
            } catch (DeletedException er) {
            }
        } else {
            System.err.println("[ ERROR ] User is not allowed to edit message.");
        }
        return channel;
    }

    /**
     * TODO: SET CHANNEL ROLE
     * Owner: Can set roles, delete and edit any message, as
     * well as read and post to the channel.
     *
     * Moderator: Can delete and edit any message, as
     * well as read and post to the channel.
     *
     * Participant: Can delete and edit their own messages, as
     * well as read and post to the channel.
     *
     * Observer: Can read messages in the channel.
     *
     * Banned: Has no access to the channel.
     *
     * The default behaviour should be that the creator of the
     * channel becomes the owner, and that inviting someone
     * puts them at the "Participant" level.
     * Also, make sure that your system satisfies the invariant:
     *
     * Every channel has at least one owner.
     */
    public void setRole(Channel channel, User user, Role role){
        if (channelUsers.containsKey(channel.name)) {  // if channel already exists
            channelUsers.get(channel.name).put(user.name.getName(), role);
        } else {
            HashMap<String, Role> userRolesInChannel = new HashMap<String, Role>();
            userRolesInChannel.put(user.name.getName(), role);
            channelUsers.put(channel.name, userRolesInChannel);
        }
    }

    // ================================================= \\
    // ===== HELPER FUNCTIONS FOR USER ROLES BELOW ===== \\
    // ================================================= \\
    private HashMap<String, Role> getUserRoles(String channelName) {
        return channelUsers.get(channelName);
    }

    /** Converts username to User object.
     *
     * @param username The username to lookup User-object from.
     * @return The result from looking up the username. If nothing, returns null.
     */
    public User userFromName(String username) {
        try {
            return userStore.lookup(username).get().value;
        } catch (Maybe.NothingException e) {
            System.err.println("[ ERROR ] Could not find user with name \"" + username + "\".");
        }
        return null;
    }

    /**
     * This should be called on every channel action, to ensure there is a owner.
     *
     * @param channel The channel to check
     * @throws NoSuchObjectException
     */
    private void assertHasOwner(Channel channel) throws NoSuchObjectException {
        HashMap<String, Role> userRoles = channelUsers.get(channel.name);

        for (Role r : userRoles.values()) {
            if (r == Role.OWNER)
                return;
        }

        throw new NoSuchObjectException("[ ERROR ] Channel has no owner.");
    }

    private boolean canPost(Channel channel, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.OWNER || userRole == Role.MODERATOR || userRole == Role.PARTICIPANT;
    }

    private boolean canEditAll(Channel channel, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.OWNER || userRole == Role.MODERATOR;
    }
    private boolean canEditOwn(Channel channel, User origin, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.OWNER || userRole == Role.MODERATOR ||
                (userRole == Role.PARTICIPANT && origin.equals(user));  // if it's user own msg
    }

    private boolean canDeleteAll(Channel channel, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.OWNER || userRole == Role.MODERATOR;
    }
    private boolean canDeleteOwn(Channel channel, User origin, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.OWNER || userRole == Role.MODERATOR ||
                (userRole == Role.PARTICIPANT && origin.equals(user));  // if it's the user's own msg
    }

    private boolean isBanned(Channel channel, User user) {
        Role userRole = channelUsers.get(channel.name).get(user.name.getName());
        return userRole == Role.BANNED;
    }
}
