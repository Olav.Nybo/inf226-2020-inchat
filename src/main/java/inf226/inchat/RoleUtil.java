package inf226.inchat;

public class RoleUtil {

    public static Role stringToRole(String role) {
        switch (role) {
            case "Owner":
                return Role.OWNER;

            case "Moderator":
                return Role.MODERATOR;

            case "Participant":
                return Role.PARTICIPANT;

            case "Observer":
                return Role.OBSERVER;

            case "Banned":
                return Role.BANNED;

            default:
                return Role.UNDEFINED;
        }
    }
}
