package inf226.inchat;

import org.owasp.encoder.Encode;

public class EncoderUtil {

    /** Symbols that are allowed behind an amp symbol.
    private final String[] behindAmpSymbol = {

    };

    /**
     * Added extra rules, replacing '/' and '&' with symbol-codes.
     * (Per.: https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)
     * (rule read 15.10.2020)
     *
     * @param toEncode The string to be encoded
     * @return
     */
    public static String forHtml(String toEncode) {
        // (1) & is replaced by &amp
        String encoded = toEncode.replace("&", "&amp");

        // (2) No '/'
        encoded = encoded.replace("/", "&#x2F");

        // Using OWASP encoding last
        encoded = Encode.forHtml(encoded);

        return encoded;
    }
}
