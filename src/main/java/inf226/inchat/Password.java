package inf226.inchat;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * In short, the new NIST guidance recommends the following for passwords:
 *
 *     A minimum of eight characters and a maximum length of at least 64 characters
 *     The ability to use all special characters but no special requirement to use them
 *     Restrict sequential and repetitive characters (e.g. 12345 or aaaaaa)
 *     Restrict context specific passwords (e.g. the name of the site, etc.)
 *     Restrict commonly used passwords (e.g. p@ssw0rd, etc.) and dictionary words
 *     Restrict passwords obtained from previous breach corpuses
 */
public final class Password {

    private final String hashedPassword;

    public Password(String password){
        this.hashedPassword = password;
    }

    /**
     * Get the hashed password.
     * @return The hashed password
     */
    public String getHashedPassword() { return hashedPassword; }

    /**
     * Returns true if the password passes the NIST requirements
     * @param p char[] of plaintext password.
     * @return boolean
     */
    public boolean isStrong(char[] p) {
        boolean b = !isSequential(p, 3) && isCorrectLength(p) && !isCommonlyUsed(p) && !isInContext(p) && !isRepetitive(p);
        HashUtil.getHasher().wipeArray(p);
        return b;
    }

    /**
     * Returns true if password contains an "In context word".
     * Such as InChat or Register.
     * @param password char[] of plaintext password.
     * @return boolean
     */
    private boolean isInContext(char[] password) {
        String[] inContextPasswords = {"InChat","inchat","Inchat","Register","register"};

        for (String word: inContextPasswords) {
            String p = Arrays.toString(password);
            if (p.contains(word)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns true if password is a commonly used password.
     * list of commonly used password found here:
     * https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-100.txt
     * @param password char[] of plaintext password.
     * @return boolean
     */
    private boolean isCommonlyUsed(char[] password) {
        String[] commonlyUsedPasswords = new String[]{"000000", "1111", "111111", "11111111", "112233", "121212", "123123", "123321", "1234", "12345", "123456", "1234567", "12345678", "123456789", "1234567890", "123qwe", "131313", "159753", "1qaz2wsx", "2000", "555555", "654321", "666666", "6969", "696969", "777777", "7777777", "987654321", "aaaaaa", "abc123", "access", "amanda", "andrew", "asdfgh", "ashley", "asshole", "austin", "baseball", "batman", "biteme", "buster", "charlie", "cheese", "chelsea", "computer", "dallas", "daniel", "dragon", "football", "freedom", "fuck", "fuckme", "fuckyou", "george", "ginger", "harley", "hockey", "hunter", "iloveyou", "jennifer", "jessica", "jordan", "joshua", "killer", "klaster", "letmein", "love", "maggie", "master", "matrix", "matthew", "michael", "michelle", "minecraft", "monkey", "mustang", "nicole", "pass", "password", "pepper", "princess", "pussy", "qazwsx", "qwerty", "qwertyuiop", "ranger", "robert", "shadow","soccer", "starwars", "summer", "sunshine", "superman", "taylor", "thomas", "zxcvbn", "zxcvbnm"};

        for (String word : commonlyUsedPasswords) {
            if (Arrays.equals(password, word.toCharArray())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks the password for 3 repetitive characters
     * !NB upper and lower case characters will be counted as equal so AaA == aaa.
     * @param password char[] of plaintext password.
     * @return boolean
     */
    private boolean isRepetitive(char[] password) {

        ArrayList<Integer> sequential = new ArrayList<>();

        int pos = 0;

        while (pos != password.length) {
            // might want to change this so upper and lower case are different values
            int val = Character.getNumericValue(password[pos]);

            if (sequential.size() == 0) {
                sequential.add(val);
            } else if (sequential.get(sequential.size()-1) == val) {
                sequential.add(val);
                if (sequential.size() == 3)
                    return true;

            } else {
                sequential.clear();
                sequential.add(val);
            }

            pos += 1;
        }
        // no repetitive sequence found.
        return false;
    }

    /**
     * Checks the password for sequential characters
     * !NB upper and lower case characters will be counted as equal so AaA == aaa.
     * @param password char[] of plaintext password.
     * @param numSeq number of sequential characters for method to return true
     * @return boolean
     */
    private boolean isSequential(char[] password, int numSeq) {


        ArrayList<Integer> sequential = new ArrayList<>();

        int pos = 0;

        while (pos != password.length) {
            // might want to change this so upper and lower case are different values
            int val = Character.getNumericValue(password[pos]);

            if (sequential.size() == 0) {
                sequential.add(val);
            } else if (sequential.get(sequential.size()-1) + 1 == val) {
                sequential.add(val);
                if (sequential.size() == numSeq)
                    return true;
            } else {
                sequential.clear();
                sequential.add(val);
            }

            pos += 1;
        }
        // no sequential sequence found.

        return false;
    }

    /**
     * Returns true if password is longer than 8 characters long.
     * @param password char[] of plaintext password.
     * @return boolean
     */
    private boolean isCorrectLength(char[] password) { return 8 <= password.length; }

    public boolean check(String password) {
        char[] passwordChars = password.toCharArray();
        boolean equal = HashUtil.verify(hashedPassword, passwordChars);

        HashUtil.getHasher().wipeArray(passwordChars);

        return equal;
    }
}
