package inf226.inchat;

public final class UserName {

    private final String name;

    public UserName(String userName) {
        this.name = userName;
    }


    public String getName() {
        return name;
    }
}
