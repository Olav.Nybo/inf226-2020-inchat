package inf226.inchat;

import de.mkammerer.argon2.Argon2Advanced;
import de.mkammerer.argon2.Argon2Factory;

/**
 * This class contains static methods for using Argon2.
 */
public class HashUtil {

    /**
     * Returns instance of an Argon2Advanced-object, which uses Argon2id.
     * Used for hashing and verifying.
     *
     * @return a Argon2Advanced object used for hashing & verifying
     */
    public static Argon2Advanced getHasher() {
        return Argon2Factory.createAdvanced(Argon2Factory.Argon2Types.ARGON2id);
    }

    /**
     * Hashes a String with Argon2id.
     *
     * @param toBeHashed The text to be hashed
     * @return A hashed string
     */
    public static String hash(char[] toBeHashed) {
        // TODO: optimize params
        int it  = 10;      // iterations
        int mem = 32768;  // memory
        int par = 1;       // parallel threads

        String hashed = getHasher().hash(it, mem, par, toBeHashed);
        getHasher().wipeArray(toBeHashed);

        return hashed;
    }

    /**
     * Wrapper method for getHasher().verify().
     *
     * @param hashedPassword The stored, hashed password
     * @param plainPassword The plaintext password
     * @return A boolean true if they match, false if not
     */
    public static boolean verify(String hashedPassword, char[] plainPassword) {
        boolean isEqual = getHasher().verify(hashedPassword, plainPassword);
        getHasher().wipeArray(plainPassword);

        return isEqual;
    }
}
