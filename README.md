# INCHAT – The INsecure CHAT application

Welcome to this second mandatory assignment of INF226.
In this assignment you will be improving the security
of a program called inChat – a very simple chat application,
in the shape of a [Jetty](https://www.eclipse.org/jetty/)
web application.

inChat has been especially crafted to contain a number
of security flaws. You can imagine that it has been
programmed by a less competent collegue, and that after
numerous securiy incidents, your organisation has decided
that you – a competent security professional – should take
some time to secure the app.

For your convenience, the task is separated into specific
exercises or tasks. These task might have been the result
of a security analysis. If you discover any security issues
beyond these tasks, you can make a note of them at the
end of this report.

For each task, you should make a short note how you solved
it – ideally with a reference to the relevant git-commits you
have made.

## Evaluation

This assignment is mandatory for the course, and counts 20%
of your final grade. The assigment is graded 0–20 points,
where you must get a minimum of 6 points in order to pass
the assignment.

## Groups

As with the previous assignments, you can work in groups of 1–3 students
on this assginment. Make sure that everyone is signed up for the group
on [MittUiB](https://mitt.uib.no/courses/24957/groups#tab-8746).
One good way to collaborate is that one person on the group makes a
fork and adds the other group members to that project.

## Getting and building the project

Log into [`git.app.uib.no`](https://git.app.uib.no/Hakon.Gylterud/inf226-2020-inchat) and make your
own fork of the project there. *Make sure your fork is private!*
You can then clone your repo to your own persion machine.

To build the project you can use Maven on the command line, or configure
your IDE to use Maven to build the project.

 - `mvn compile` builds the project
 - `mvn test` runs the tests. (There are only a few unit test – feel free to add more).
 - `mvn exec:java` runs the web app.

Once the web-app is running, you can access it on [`localhost:8080`](http://localhost:8080/).

## Handing in the assignment

Before you hand in your assignment, make sure that you
have included all dependencies in the file `pom.xml`, and
that your program compiles and runs well. One good way
to test this is to make a fresh clone from the GitLab repo,
compile and test the app.

Once you are done, you submit the assignment on
[`mitt.uib.no`](https://mitt.uib.no/) as a link to your
fork – one link per group. This means you should not commit to the
repository after the deadline has passed. Include the commit hash
of the final commit (which you can find `git log`, for instance) in
your submission on MittUiB.

Remember to make your fork accessible to the TAs and lecturer. You can do
this from GitLab's menu, "Settings" → "Members".
Add the following people as developers:

 - Alba Gullerud,
 - Kenneth Fossen,
 - Jonas Møller,
 - Erlend Nærbø ,
 - Benjamin Chetioui, and
 - Håkon Gylterud

## Updates

Most likely the source code of the project will be updated by Håkon
while you are working on it. Therefore, it will be part of
your assignment to merge any new commits into your own branch.

## Improvements?

Have you found a non-security related bug?
Feel free to open an issue on the project GitLab page.
The best way is to make a separate `git branch` for these
changes, which do not contain your sulutions.

(This is ofcourse completely volountary – and not a graded
part of the assignment)

If you want to add your own features to the chat app - feel free
to do so! If you want to share them, contact Håkon and we can
incorporate them into the main repo.

## Tasks

The tasks below has been separated out, and marked with their *approximate* weight. Each task has a section called "Notes" where you can
write notes on how you have solved the task.

### Task 0 – Authentication (4 points)

The original authentication mechanisms of inChat was so insecure it
had to be removed immediately and all traces of the old passwords
have been purged from the database. Therefore, the code in
`inf226.inchat.Account`, which is supposed to check the password,
always returns `true`.

#### Task 0 – Part A

*Update the code to use a secure password authentication method – one
of the methods we have discussed in lecture.*

Any data you need to store for the password check can be kept in the `Account` class, with
appropriate updates to `storage.AccountStorage`. Remember that the `Account` class is *immutable*.
Any new field must be immutable and `final` as well.

**Hint**:

 - An implementation of `scrypt` is already included as a dependency in `pom.xml`.
   If you prefer to use `argon2`, make sure to include it as well.


### Task 0 – Part B

Create two new, immutable, classes `UserName` and `Password` in the
inf226.inchat package, and replace `String` with these
classes in User and Account classes and other places in
the application where it makes sense.

Decide on a set of password criteria which satisfies
the NIST requirements, and implement these as invariants
in the Password class, and check these upon registration.

### Task 0 – Part C

*While the session cookie is an unguessable UUID, you must set the
correct protection flags on the session cookie.*

#### Notes – task 0

When we first worked on this task, we did not make a new branch for it. Later, we did however create a branch for the login system.
However, the first few commits of master should be considered included in the (*later made*) branch "auth".

##### Part A
We decided to opt for `argon2`, more specifically `argon2id`. The reading material for this is included in sources.
> Wherever possible you should use an Argon2id implementation.

(https://security.stackexchange.com/a/197550)
<br>
Approach:
1. We created a class `HashUtil` to separate the argon2 logic. This class provided simple wrapper functions
for us to use.
2. We implemented the method `checkPassword` using `HashUtil`, and decided to store the password hash in `Account`.
    * This required that the constructor accepts a `hashedPassword` argument. This again required us to edit some 
    lines in `AccountStorage`.
    * We want to store the password hash in the database __only__. For now, the solution with storing the hash will do.
3. The changes made in `AccountStorage` made it logical to create a column `hashedPassword` in the database.
 We therefore changed the SQL-calls to accomodate for this.

##### Part B
Approach:
1. We created the class `Password` and implemented methods that check if the password meats the NIST requirements.  
2. We then moved `checkPassword` and getHashedPassword from `Account` to `Password`, so everything gets handled in `Password`.  
3. Then we replaced String in `Account` where it made sense and checked the password in the `register()` method in `Inchat`
4. Created the class `UserName` and implemented a getter then replaced String in `User` where it made sense.

##### Part C
We called the method `setSecure` on the session cookie with the argument flag `true`.

##### Miscellaneous
We repaired the main methods for the login system, `InChat.login()` & `InChat.register()`.

`InChat.login()`
* Now actually checks that the account trying to log in specified the correct password.

`InChat.register()`
* Checks that all usernames are unique - no users can have same username.

__Known bugs / questions__:
* There are two password-fields, however, only one of them seems to be passed along in `Handler.java:109`.

### Task 1 – SQL injection (4 points)

The SQL code is currently wildly concatenating strings, leaving
it wide open to injection attacks.

*Take measures to prevent SQL injection attacks on the application.*

#### Notes – task 1

To prevent SQL-injection we used PreparedStatements where there is user input in an SQL command.
PreparedStatements forces the user-input to be handled as a parameter rather than part of the SQL command, thus avoiding SQL injection.

### Task 2 – Cross-site scripting (4 points)

The user interface is generated in `inf226.inchat.Handler`. The current
implementation is returning a lot of user data without properly
escaping it for the context it is displayed (for instance HTML body).

*Take measures to prevent XSS attacks on inChat.*

**Hint**: In addition to the books and the lecture slides, you should
take a look at the [OWASP XSS prevention cheat sheet](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)

#### Notes – task 2
1. We made a class `EncoderUtil.java` that wraps around the OWASP-library function `Encode.forHtml()`.
We made this wrapper in order to add some custom rules, such as replacing "/" and "&" with their
respective codes.
2. We made tests for `EncoderUtil.java`. The test-class is names `EncoderTest.java`. 
3. Incorporated `EncoredUtil`'s methods in `Handler.java` to prevent XSS-attacks. Used where raw input is concatenated
with html-code.
4.  Used OWASP-library's different methods for handling html- and js-code.
    * `Encode.forHtmlContent()`
    * `Encode.forHtmlAttribute()`
    * `Encode.forJavaScript()`

### Task 3 – Cross-site request forgery (1 point)

While the code uses UUIDs to identify most objects, some
form actions are still susceptible to cross-site request forgery attacks
(for instance the `newmessage` and the `createchannel` actions.)

*Implement anti-CSRF tokens or otherwise prevent CSRF on
the vulnerable forms.*

**Hint:** it is OK to use the session cookie as such a token.

#### Notes – task 3

Here you write your notes about how this task was performed.

1. Added OWASP library for secure encoding, see "Sources".
2. Encoded data when accepting data within html elements / in tags.  
3. Set the `SameSite flag` for the response to `Strict`
4. Added `crsftokens` to forms: `createchannel`, `joinchannel` and `editmessage`
(We intended to add more tokens to the other vulnerable forms, but ran out of time.)

### Task 4 – Access control (5 points)

inChat has no working access control. The channel side-bar has a form
to set roles for the channel,
but the actual functionality is not implemented.

In this task you should implement access control for inChat.

 - Identify which actions need access control, and decide
   on how you want to structure the access control.
 
Connect the user interface in the channel side-bar to your
access control system so that the security roles work as
intended. The security roles in a channel are:

 - *Owner*: Can set roles, delete and edit any message, as
   well as read and post to the channel.
 - *Moderator*: Can delete and edit any message, as
   well as read and post to the channel.
 - *Participant*: Can delete and edit their own messages, as
   well as read and post to the channel.
 - *Observer*: Can read messages in the channel.
 - *Banned*: Has no access to the channel.

The default behaviour should be that the creator of the
channel becomes the owner, and that inviting someone
puts them at the "Participant" level.
Also, make sure that your system satisfies the invariant:

 - Every channel has at least one owner.

 **Hint:** The InChat class is best suited to implement the
 access control checks since it in charge of all the operations
 on the chat. Implement a "setRole" method there, and add
 security checks to all other methods.

#### Notes – task 4

We did not manage to test our solution to this task, as we did not implement
binder between access logic and the UI. We did however, lay out our general solution to this problem.

1. Created `Role.java` to list all available roles an entity can have in a channel
2. Created `RoleUtil.java` to access useful methods for roles
3. Created a `HashMap` in `InChat.java` to keep track of which users is in a channel, and which roles they have
4. Created helper methods to support the `HashMap` from (3)
5. Implemented access control in the different methods, where it is needed


### Task ω – Other security holes (2 points)

There are more security issues in this web application.
Improve the security of the application to the best of your
ability.

A note about HTTPS: We assume that inChat will be running
behind a reverse proxy which takes care of HTTPS, so you
can ignore issues related HTTPS.

#### Notes – task ω

Added `required` to html tags as password and username was marked as required.
This problem also rendered two input fields for passwords useless, as they were not __both__ required.

This issue where the two password is not checked up against each other still persists however.
It could be fixed by checking both of the argon2 hashes up against each other, the hashes should be equal in order to be allowed to register.

## Sources
1. https://security.stackexchange.com/a/197550
    * Read 12.10.2020
    * Reading material for choosing algorithm to use.
    
2. https://github.com/phxql/argon2-jvm
    * Read 12.10.2020
    * Library for argon2.
    * A binding on this repository:
        * https://github.com/P-H-C/phc-winner-argon2
        
3. https://spycloud.com/new-nist-guidelines/
    * Read 13.10.2020
    * List for the NIST requirements.
        
3. https://owasp.org/www-community/controls/SecureFlag
    * Read 14.10.2020
    * Reading material for secure flags for cookies.
    
4. https://github.com/OWASP/owasp-java-encoder
    * Read 14.10.2020
    * Library for encoding strings for html.
    
5. https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-Credentials/10-million-password-list-top-100.txt
    * Read 18.10.2020
    * List of commonly used password in Task 0.